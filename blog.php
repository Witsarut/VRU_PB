<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
  
</head>
<body>

<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
        <div id="templatemo_search">
            <form action="#" method="get">
              <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a href="index.php">แบบประเมิณ</a></li>
            <li><a href="about.php" class="selected">บทเรียน</a>
            </li>
            <li><a href="portfolio.php">ผู้ดูแลระบบ</a>
            </li>
            <li><a href="blog.php">เว็บบอร์ด</a></li>
            <li><a href="contact.php">แบบทดสอบ</a></li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>


<div id="templatemo_main">
    <td width="50"><h2>กระทู้แสดงความคิดเห็น</h2></td>
 <?php
    $objConnect = mysql_connect("localhost","root","") or die ("Error Connect to Database");
    $objDB = mysql_select_db("vru-database");
    mysql_query("SET NAMES UTF8");
    $strSQL = "SELECT * FROM webboard ";
    $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
    $Num_Rows = mysql_num_rows($objQuery);
    
$Per_Page = 20;   // Per Page
$Page = (isset($_GET['Page']));
if (!isset($_GET['Page'] ))              //if (!$_GET["Page"])
{    
  $Page=1;
}

 $Prev_Page = $Page-1;
 $Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);

if($Num_Rows<=$Per_Page)
{
  $Num_Pages =1;
}
  else if(($Num_Rows % $Per_Page)==0)
{
  $Num_Pages =($Num_Rows/$Per_Page) ;
}
else

{
  $Num_Pages =($Num_Rows/$Per_Page)+1;
  $Num_Pages = (int)$Num_Pages;
}

  $strSQL .=" order  by QuestionID DESC LIMIT $Page_Start , $Per_Page";
  $objQuery  = mysql_query($strSQL);
?>

<table width="915" border="3">
 
    <tr bgcolor="#669933">
        <a href="NewQuestion.php"><h5><font color="blue"><strong>ตั้งคำถามใหม่</strong></font></h5></a>
        <td colspan="6"><div align="center"><strong><h4><font color="#FFCC00"><marquee>ระบบกระทู้ถาม-ตอบ</marquee></font></h4></strong></td>
    </tr>

    <tr bgcolor='#FFFF66'>
        <th width="70"><div align="center"><font color="black">รหัสคำถาม</font></div></th>
        <th width="250"><div align="center"><font color="black">คำถาม</font></div></th>
        <th width="90"><div align="center"><font color="black">ชื่อ</font></div></th>
        <th width="130"><div align="center"><font color="black">วันที่ตั้งคำถาม</font></div></th>
        <th width="50"><div align="center"><font color="black">ครั้งที่ดู</font></div></th>
        <th width="50"><div align="center"><font color="black">ครั้งที่ตอบ</font></div></th>
    </tr>

<?php
  while($objResult = mysql_fetch_array($objQuery))
{

?>

 
  <tr bgcolor ='#F8F8FF'>
      <td><font color ='black'><div align='center'><?php echo $objResult['QuestionID'];?></div></font></td>
      <td><a href='ViewWebboard.php?QuestionID=<?php echo $objResult["QuestionID"];?>'><font color="blue"><?php echo $objResult['Question'];?></font></a></td>
      <td align ='center'><font color='black'><?php echo $objResult["Name"];?></font></td>
      <td><font color ='black'><div align='center'><?php echo $objResult['CreateDate'];?></div></font></td>
      <td align='center'><font color ='black'><?php echo $objResult['View'];?></font></td>
      <td align='center'><font color ='black'><?php echo $objResult['Reply'];?></font></td>
  </tr>

<?php
  }
?>
</table>
      <br>
          <font color ='black'>Total</font><font color="black"> <?php echo  $Num_Rows;  ?></font> <font color='black'><font color ='black'>Record</font> : <?php echo $Num_Pages;?> Page :
<?php
  if($Prev_Page)
{
    echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page'><< Back</a> ";
}

for($i=1; $i<=$Num_Pages; $i++){
    if($i != $Page)
{
  echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> ]";
}
else
{
  echo "<b> $i </b>";
}

}
  if($Page!=$Num_Pages)
{
  echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page'>Next>></a> ";
}
  mysql_close($objConnect);
?>

    <br>   
        <br class="cleaner" />
</div> <!-- end of main -->

<div id="templatemo_footer">
        <br><br><br><br>     
</div>

    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>

<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
</body>
</html>