<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>

<script language="JavaScript">
	
	function checkVal(){
		
			
		if(document.getElementById('txtQuestion').value == false){
			alert('กรุณากรอกคำถาม');
			document.getElementById('txtQuestion').focus();
			return false;
		}
		
		for(i=1;i<=4;i++){
			if(document.getElementById('txtAnswer'+i).value == false){
				alert('กรุณากรอกคำตอบที่'+i);
				document.getElementById('txtAnswer'+i).focus();
				return false;
				break;	
			}	
		}
		
		if(document.getElementById('txtAnswerTrue').value == false){
			alert('กรุณากรอกคำตอบที่ถูกต้อง');
			document.getElementById('txtAnswerTrue').focus();
			return false;	
		}else{
			var permiss=0;
			for(i=1;i<=4;i++){
				if(document.getElementById('txtAnswerTrue').value == document.getElementById('txtAnswer'+i).value){
					permiss=1;
					break;
				}
			}
			if(permiss==0){
				alert('คำตอบไม่เหมือนกันกรุณากรอกใหม่');
				document.getElementById('txtAnswerTrue').focus();
				return false;	
			}	
		}
	}
</script>

<?php
	if($_SESSION['Admin'] == ""){
		echo "<script>";
		echo "alert('กรุณาล๊อคอินเข้าสู่ระบบ');";
		echo "window.location='index.php';";
		echo "</script>";
	}else{

	$Act=$_GET['Act'];
	switch($Act){
		case 'Add'	:	 	$Cours=explode("#", $_POST['txtcourse']);
							$Question=$_POST['txtQuestion'];
							$Answer=$_POST['txtAnswer'];
							$AnswerTrue=$_POST['txtAnswerTrue'];
							$insertQuestion=insert("question","Course,Question,AnswerTrue,subject_id,ArticleID","'".$Cours[0]."','".$Question."','".$AnswerTrue."','".$Cours[1]."','".$Cours[1]."'");
							if($insertQuestion){
								$QuestionID=mysql_insert_id();
								for($i=0;$i<4;$i++){
									$insertAnswer=insert("answer","QuestionID,Answer","'".$QuestionID."','".$Answer[$i]."'");	
								}
								echo "<script>";
								echo "alert('ระบบทำการเพิ่มคำถาม $Question เรียบร้อย');";
								echo "window.location='detail_question.php';";
								echo "</script>";	
							}
		break;	
	}
?>

<?php
	function query(){
		$mydata=mysql_query("SELECT * FROM article");	
		while ($record=mysql_fetch_array($mydata)){
			echo '<option value="'.$record['Article'].'#'.$record['ArticleID'].'">' .$record['Article']. '</option>';
		}
	}
?>

<div id="container-left">

<form action="?Act=Add" method="post">  
	
        <table  class="table-full margin">
		
			<tr>
				<td colspan="4"><div class="title green"><font color="black">&nbsp;เพิ่มคำถาม</font></div></td>
			</tr>
		  
		  <tr>
			<td valign="top"><strong><font color="black">&nbsp;&nbsp;กรุณาเลือกวิชา</font></strong> 
			<td valign="top">
				<select name="txtcourse">
					<?php query() ?>
				</select>
		  </tr>
		  
		  <tr>
            <td width="20%"><font color="black">&nbsp;&nbsp;คำถาม</font></td>
            <td colspan="4"><input type="text" id="txtQuestion" class="txtboxl" name="txtQuestion" value="" /></td>
          </tr>
          
		  <tr>
			<td colspan="4">
			<?php for($i=1;$i<=4;$i++){ ?>
			<font color="black">&nbsp;&nbsp;คำตอบที่</font><?php=$i;?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type='text' id='txtAnswer<?php=$i;?>' class="txtbox" name='txtAnswer[]' value=''><br>	
				<?php } ?>
          	</td>
          </tr>
		  <br />
          
		  <tr>
			<td><font color="black">&nbsp;&nbsp;คำตอบที่ถูกต้อง</font></td>
            <td width="63%"><input type="text" id="txtAnswerTrue" class="txtbox" name="txtAnswerTrue" value="" /></td>
            <td width="5%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
          </tr>
		  
          <tr>
            <td>&nbsp;</td>
            <td><input type="submit" class="btngreen" value="เพิ่มคำถาม" onclick="return checkVal();" /><input type="reset" class="btnred" value="ล้างข้อมูล" /></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table> 
    </form>  		
    
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

 <div id="container-right">
 <?php include ("/menu.php"); ?>    
 </div>

<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
<?php
	}
?>
    
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
</body>
</html>