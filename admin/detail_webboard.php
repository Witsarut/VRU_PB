<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>
<?php
    if($_SESSION['Admin'] == ""){
        echo "<script>";
        echo "alert('กรุณาล๊อคอินเข้าสู่ระบบ');";
        echo "window.location='index.php';";
        echo "</script>";
    }else{
        
    $Act=$_GET['Act'];
    switch($Act){
        case 'Del'  :    $QuestionID=$_GET['QuestionID'];
                            $Question=$_GET['Question'];
                            $delectQuestion=delete("webboard","QuestionID='".$QuestionID."'");
                                
                                echo "<script>";
                                echo "alert('ระบบทำการลบ $Question เรียบร้อย');";
                                echo "window.location='detail_webboard.php';";
                                echo "</script>";   
                                
        break;
    }
?>  
<div id="container-left">
    <table  class="table-full margin">
        <br />
        <tr>
            <td colspan="4"><div class="title green"><font color="black">&nbsp;รายการกระทู้ถาม-ตอบ</font></div></td>
        </tr>
        
    <tr>
        <td width="5%"><div class="center"><font color="black">&nbsp;ลำดับ</font></div></td>
        <td width="20%"><div class="margin"><font color="black">หัวข้อกระทู้</font></div></td>     
        <td width="10%"><div class="center"><font color="black">ชื่อ</font></div></td>
        <td width="10%"><div class="center"><font color="black">วันที่ตั้งคำถาม</font></div></td>
        <td width="5%"><div class="center"><font color="black">ลบ</font></div></td>
        
    </tr>
    <tr>
        <td colspan="6"><div class="line"></div></td>
    </tr>   
    
    <?php
        $select=select("webboard","1=1");
        $no=0;
        while($web=mysql_fetch_array($select)){
            $no++;
    ?>
    <tr>
        <td><div class="center"><font color="black"><?=$no;?></font></div></td>
        <td><div class="margin"><font color="black"><?php echo $web['Question'];?></font></div></td>
        <td><div class="center"><font color="black"><?php echo $web['Name'];?></font></div></td>
        <td><div class="center"><font color="black"><?php echo $web['CreateDate'];?></font></div></td>
    <td><div class="center"><img src="images/icon/tools/Trash.png" class="cusor" onclick="window.location='?Act=Del&QuestionID=<?=$web['QuestionID'];?>&Question=<?=$web['Question']?>';" /></div></td>
    </tr>
    
    <tr>
        <td colspan="6"><div class="line"></div></td>
    </tr>
        <?php } ?>  
        
    </table>
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

 <div id="container-right">
 <?php include ("/menu.php"); ?>    
 </div>

<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
<?php
    }
?>    
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
</body>
</html>