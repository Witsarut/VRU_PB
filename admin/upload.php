<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>
	
	<?php
		function query(){
			$mydata=mysql_query("SELECT * FROM article");	
			while ($record=mysql_fetch_array($mydata)){
				echo '<option value="'.$record['Article']. '">' .$record['Article']. '</option>';
			}
		}
	?>

	<div id="container-left">
		<form action="save_upload.php" method="post" enctype="multipart/form-data">
			<table class="table-full margin">
				<tr>
					<td colspan="4"><div class="title green"><font color="black">&nbsp;อัพโหลดวีดีโอ/ไฟล์</font></div></td>
				</tr>
				<br />
			</table>
			<table class="table-full margin">
				<tr>
					<td colspan="2"><font color="black">&nbsp;&nbsp;&nbsp;&nbsp;วิชา :</font>
					<select name="txtName">
						<?php query() ?>
					</select></td>
				</tr>
				</table>
				
				<table class="table-full margin">	
					<tr>
						<td colspan="2"><font color="black">&nbsp;&nbsp;วีดีโอ :</font>
						<input type="file" name="file"></td>
					</tr>
				</table>
				
				<table class="table-full margin">
				<tr>
					<td colspan="2"><font color="black">&nbsp;ผู้สอน :</font>
					<select name="txtteacher">
						<option>ชุมพล ปทุมมาเกษร</option>
						<option>โยษิตา เจริญศิริ</option>
						<option>เฉลิมพล แก้วเทพ</option>
						<option>ธณพร พยอมใหม่</option>
						<option>วิวัฒิน์ คลังวิจิตร</option>
						<option></option>
					</select></td>
				</tr>
				</table>
				
				<tr>
					<td colspan="2">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input name="btn-upload" type="submit" value="ตกลง" /></td>
				</tr>
			<table class="table-full margin">
				<tr>	
					<td colspan="4" class="center"><font color="black">สามารถอัพโหลดไฟล์ต่างๆ เช่น (PDF, DOC,VIDEO, MP3,MP4 ZIP,etc....)</font></td>
				</tr>
			</table>
			
	
		</form>	
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

 <div id="container-right">
 <?php include ("/menu.php"); ?>    
 </div>

<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
    
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
</body>
</html>