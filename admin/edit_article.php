<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<script src="module/editor/jscripts/tiny_mce/tiny_mce.js"></script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>

<script>
    function checkVal(){
        if(document.getElementById('txtArticle').value ==''){
            alert('กรุณากรอกชื่อเรืองบทความ');
            document.getElementById('txtArticle').focus();
            return false;
        }
    }    

</script>

<?php
    if($_SESSION['Admin'] == ""){
        echo "<script>";
        echo "alert('กรุณาล๊อคอินเข้าสู่ระบบ');";
        echo "window.location='index.php';";
        echo "</script>";
    }else{
        
    include("editor.php");
    $Act=$_GET['Act'];
    switch($Act){
        case "Update"   :     $ArticleID=$_GET["ArticleID"];
                                $Article=$_POST["txtArticle"];
                                $Description=$_POST["txtDescription"];
                                $UpdateDate=$_POST["dtUpdateDate"];
                                $Signature=$_POST["txtSignature"];
            $update=update("article","Article='".$Article."',Description='".$Description."',UpdateDate='".$UpdateDate."',Signature='".$Signature."'","ArticleID='".$ArticleID."'");
                            if($update){
                                    echo "<script>";
                                    echo "alert('ระบบทำการแก้ไขข้อมูล $Article เรียบร้อย');";
                                    echo "window.location='detail_article.php';";
                                    echo "</script>";   
                                }
        break;  
    }
    $ArticleID=$_GET["ArticleID"];
    $select=select("article","ArticleID='".$ArticleID."'");
    $art=mysql_fetch_array($select);
?>
<div id="container-left">
<form  action="?Act=Update&ArticleID=<?=$ArticleID;?>" method="post">
	<table width="93%" class="table-full margin">
		<br />
		<tr>
			<td colspan="4"><div class="title green"><font color="black">&nbsp;แก้ไขรายวิชา</font></div></td>
		</tr>
	   <td>&nbsp;</td>
        <tr>
            <td width="10%"><font color="black">&nbsp;ชื่อวิชา</font></td>
            <td colspan="30%"><input type="text" id="txtArticle" name="txtArticle" class="txtboxl" value="<?php echo $art['Article'];?>" /></td>
        </tr>
        <tr>
            <td width="10%"><font color="black">&nbsp;รหัสวิชา</font></td>
            <td width="30%"><input type="text" id="txtDescription" name="txtDescription" class="txtbox"  value="<?php echo $art['Description'];?>"/></td>
        </tr>

<tr>
    <td><font color="black">&nbsp;แก้ไขวันที่</font></td>
    <td width="31%"><input type="date" class="txtbox" name="dtUpdateDate" value="<?php echo date("Y-m-d");?>" /></td>
    <td><font color="black">ผู้เขียน</font></td>
    <td width="45%"><input type="text" class="txtbox" name="txtSignature" value="Administator" /></td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td><input type="submit" class="btngreen" value="ตกลง" onclick="return checkVal();" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
 </table>
</form>
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

 <div id="container-right">
 <?php include ("/menu.php"); ?>    
 </div>

<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
<?php } ?>
</body>
</html>