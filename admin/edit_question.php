<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<script src="module/editor/jscripts/tiny_mce/tiny_mce.js"></script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>

<script>
    function checkVal(){
        if(document.getElementById('txtQuestion').value == ''){
            alert('กรุณากรอกคำถาม');
            document.getElementById('txtQuestion').focus();
            return false;
        }
        
        for(i=1;i<=4;i++){
            if(document.getElementById('txtAnswer'+i).value == ''){
                alert('กรุณากรอกคำตอบที่'+i);
                document.getElementById('txtAnswer'+i).focus();
                return false;
                break;  
            }   
        }
        
        if(document.getElementById('txtAnswerTrue').value ==''){
            alert('กรุณากรอกคำตอบที่ถูกต้อง');
            document.getElementById('txtAnswerTrue').focus();
            return false;   
        }else{
            var permiss=0;
            for(i=1;i<=4;i++){
                if(document.getElementById('txtAnswerTrue').value == document.getElementById('txtAnswer'+i).value){ 
                    permiss=1;
                    break;
                }
            }
            if(permiss==0){
                alert('คำตอบไม่เหมือนกันกรุณากรอกใหม่');
                document.getElementById('txtAnswerTrue').focus();
                return false;   
            }   
        }
    }
</script>

<?php
    if($_SESSION['Admin'] == ""){
        echo "<script>";
        echo "alert('กรุณาล๊อคอินเข้าสู่ระบบ');";
        echo "window.location='index.php';";
        echo "</script>";
    }else{
        
    $QuestionID=$_GET['QuestionID'];
    $select=select("question","QuestionID='".$QuestionID."'");
    $que=mysql_fetch_array($select);
    
    $Act=$_GET['Act'];
    switch($Act){
        case 'Update'   :     $QuestionID=$_GET['QuestionID'];  
                                $Question=$_POST['txtQuestion'];
                                $AnswerID=$_POST['hdAnswerID'];
                                $Answer=$_POST['txtAnswer'];
                                $AnwerTrue=$_POST['txtAnswerTrue'];
                                
                                $updateQuestion=update("question","Question='".$Question."',AnswerTrue='".$AnwerTrue."'","QuestionID='".$QuestionID."'");
                                    if($updateQuestion){
                                    for($i=0;$i<count($AnswerID);$i++){
                                $updateAnswer=update("answer","Answer='".$Answer[$i]."'","AnswerID='".$AnswerID[$i]."'");   
                                    }
                                    echo "<script>";
                                    echo "alert('ระบบทำกการแก้ไข $Question เรียบร้อย');";
                                    echo "window.location='detail_question.php';";
                                    echo "</script>";   
                                }
        break;  
    }
?>

<div id="container-left">
<form action="?Act=Update&QuestionID=<?=$QuestionID;?>" method="post">
    <table class="table-full margin">
        <br />
        <tr>
            <td colspan="4"><div class="title green"><font color="black">&nbsp;เพิ่มคำถาม</font></div></td>
        </tr>
        
        <tr>
            <td width="15%"><font color="black">&nbsp;คำถาม</font></td>
            <td colspan="3"><input type="text" id="txtQuestion" class="txtboxl" name="txtQuestion" value="<?=$que['Question'];?>" /></td>             
        </tr>  

    <tr>
            <td colspan="4">
            <?php
            $selectAns=select("answer","QuestionID='".$QuestionID."'");
            $i=0;
            while($ans=mysql_fetch_array($selectAns)){
                $i++;
            ?><font color="black">คำตอบที่<?=$i;?></font>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <input type="hidden" name="hdAnswerID[]" value="<?=$ans['AnswerID'];?>" />
                <input type="text" id="txtAnswer<?=$i?>" class="txtbox" name="txtAnswer[]" value="<?=$ans['Answer'];?>"><br>    
            <?php } ?>
            </td>
          </tr>

          <tr>
            <td><font color="black">คำตอบที่ถูกต้อง</font></td>
            <td width="63%"><input type="text" id="txtAnswerTrue" class="txtbox" name="txtAnswerTrue" value="<?=$que['AnswerTrue'];?>" /></td>
            <td width="5%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
          </tr>
   
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" class="btngreen" value="แก้ไขคำถาม" onclick ="return checkVal();"/><input type="reset" class="btnred" value="ล้างข้อมูล" /></td>
       </tr>

    </table>    
</form>    
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

 <div id="container-right">
 <?php include ("/menu.php"); ?>    
 </div>

<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
<?php } ?>
</body>
</html>