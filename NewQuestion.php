<?php
    error_reporting(0);
    session_start();
    include ("module/inc/php/config.inc.php");
    include ("module/inc/php/function.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
</head>
<body>
<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#">Green Jelly</a></h1>
        </div>
      
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a><marquee>มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม (กระทู้ถาม- ตอบ) </marquee></a></li>
         </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>

<?php
$objConnect = mysql_connect("localhost","root","") or die("Error Connect to Database");
$objDB = mysql_select_db("vru-database");
mysql_query("SET NAMES UTF8");

if(isset($_GET["Action"])){     //if($_GET['Action'] == 'Save'){}
    //*** Insert Question ***//
    $strSQL = "INSERT INTO webboard ";
    $strSQL .="(CreateDate,Question,Details,Name) ";
    $strSQL .="VALUES ";
    $strSQL .="('".date("Y-m-d H:i:s")."','".$_POST["txtQuestion"]."','".$_POST["txtDetails"]."','".$_POST["txtName"]."') ";
    $objQuery = mysql_query($strSQL);
    
    header("location:blog.php");
}
?>


<div id="container-left">
<form action="NewQuestion.php?Action=Save" method="post" name="frmMain" id="frmMain"> 
<table width="95%" align="center" border="1"  cellspacing="0">
<br>
<tr bgcolor="#669933">
    <td colspan="5"><div align="center"><strong><h4><font color="black">ระบบกระทู้ถาม-ตอบ</font></h4></strong></div></td>
</tr>
 
<tr>
    <td width="15%" align ='center' bgcolor ="#FFFF66"><font color="black"><strong>คำถาม</strong></font></td>
   <td bgcolor ="FFFF99"><input name="txtQuestion" type="text" id="txtQuestion" value="" size="100"></td>
</tr>

<tr>
    <td align="center" bgcolor="#FFFF66" width="100"><font color="black"><strong>รายละเอียด</strong></font></td>
    <td bgcolor="#FFFF99"><textarea name="txtDetails" cols="80" rows="5" id="txtDetails"></textarea></td>
</tr>

<tr>
    <td align="center" bgcolor="#FFFF66" width="78"><strong><font color="black">ชื่อ</font></strong></td>
    <td bgcolor="#FFFF99" width="647"><input name="txtName" type="text" id="txtName" value="" size="50"></td> 
</tr>

    </table>
<br>
<center><input name="btnSave" type="submit" id="btnSave" value="ส่งคำถาม"></center>

</form>

        <br class="cleaner" />
</div> <!-- end of main -->

 
<div id="templatemo_footer">
        <br><br><br><br>     
</div>
    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>
<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>
</body>
</html>