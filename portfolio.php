<?php
	error_reporting(0);
	session_start();
	include ("module/inc/php/config.inc.php");
	include ("module/inc/php/function.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
  
</head>


<script>
    function checkVal(){
        if(document.getElementById('txtUsername').value == ''){
            alert('กรุณากรอกชื่อผู้ดูแลระบบ');
            document.getElementById('txtUsername').focus();
            return false;
        }
        if(document.getElementById('pwPassword').value == ''){
            alert('กรุณากรอกรหัสผ่าน');
            document.getElementById('pwPassword').focus();
            return false;
			}
    }
</script>

<body>

<?php
	$arrUsername = array("admin");
	$arrPassword = array("admin");
	$Act = $_GET['Act'];
	switch($Act){
		case 'check'	:	$Username=$_POST['txtUsername'];
							$Password=$_POST['pwPassword'];
								for($i=0; $i<count($arrUsername); $i++){
									if($Username == $arrUsername[$i] && $Password == $arrPassword[$i]){
										$_SESSION['Admin']="Administator";
										break;	
									}else{
										continue;	
									}
								}
								if($_SESSION['Admin']!= ""){
									echo "<script>";
									echo "alert('ยินดีต้อนรับเข้าสู่ระบบ');";
									echo "window.location='admin/add_article.php';";
									echo "</script>";	
								}else{
									echo "<script>";
									echo "alert('Username นี้ไม่มีอยู่ในระบบ');";
									echo "window.location='portfolio.php';";
									echo "</script>";										
								}
						break;	
					}
?>

<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
        <div id="templatemo_search">
            <form action="#" method="get">
              <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a href="index.php">แบบประเมิณ</a></li>
            <li><a href="about.php">บทเรียน</a>
            </li>
            <li><a href="portfolio.php" class="selected">ผู้ดูแลระบบ</a>
            </li>
            <li><a href="blog.php">เว็บบอร์ด</a></li>
            <li><a href="contact.php">แบบทดสอบ</a></li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>


<div id="templatemo_main">
    <td width="50"><h2>ผู้ดูแลระบบ</h2></td>
 
 
  <form action="?Act=check" method="post">
       <table class ="table-full">  
            <tr>
                <td width ="31%">&nbsp;</td>
                <td colspan="2"><div class ="green"><font color="black">ล็อกอินผู้ดูแลระบบ</font></div></td>
                <td width="37%">&nbsp;</td>
            </tr>     
             
             <tr>
                <td>&nbsp;</td>
                <td width ="14%"><font color="black">ชื่อผู้ดูแลระบบ</font></td>
                <td width ="18%"><input type ="text" id ="txtUsername" name ="txtUsername" value ="" /></td>
                <td>&nbsp;</td>
             </tr>
             
             <tr>
                <td>&nbsp;</td>
                <td><font color="black">รหัสผ่าน</font></td>
                <td><input type ="password" id ="pwPassword" name="pwPassword"  value ="" /></td>
                <td>&nbsp;</td>
             </tr>   

             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><input type ="submit" value="เข้าสู่ระบบ" onclick="return checkVal();" /><input type="reset" value="ล้างข้อมูล" /></td>
                <td>&nbsp;</td>
            </tr>   
        </table>     
    </form>
   
 
<br>   
        <br class="cleaner" />
</div> <!-- end of main -->

<div id="templatemo_footer">
        <br><br><br><br>     
</div>

    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>

<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>

</body>
</html>