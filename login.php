<?php
    session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
  
</head>
<body>

<?php
    $Act=$_GET['Act'];
    switch($Act){
        case 'Login'  :  $Username=$_POST['txtLogUsername'];
                         $Password=$_POST['pwLogPassword'];
                    $num_rows=num_rows("member","Username='".$Username."' AND Password='".$Password."'");
                    if ($num_rows == 1){
                        $_SESSION['Username']=$Username;
                        echo "<script>";
                        echo "alert ('ยินดีต้อนรับคุณ $Username');";
                        echo "window.location='frist_test.php';";
                        echo "</script>";
                }else{  
                        echo "<script>";
                        echo "alert('ผู้ใช้นี้ไม่มีอยู่ในระบบ');";
                        echo "window.location='contact.php';";
                        echo "</script>";
                }
            break;
            
            case 'Logout'  : session_destroy();
                        echo "<script>";
                        echo "alert('คุณได้ออกจากระบบเรียบร้อยแล้ว');";
                        echo "window.location='index.php';";
                        echo "</script>";

            break;
    }

    if($_SESSION['Username'] == ''){    
?>

<form action="?Act=Login" method="post">
<table class="float-right">
  <tr>
    <td><font color="white">ชื่อผู้ใช้</font></td>
    <td><input type="text" id="txtLogUsername" name="txtLogUsername" value="" /></td>
    <td><font color="white">รหัสผ่าน</font></td>
    <td><input type="password" id="txtLogPassword" name="pwLogPassword" value="" /></td>
    <td><input type="submit" value="เข้าสู่ระบบ" /></td>
    <td><input type="button" value="ออกจากระบบ" onclick="window.location='index.php';"/></td>
    <td width="26%">&nbsp;</td>
  </tr>
</table>
</form>
<?php }else{ ?>
<table class="float-right">
  <tr>
    <td><font color="white">ยินดีต้อนรับ</font></td>
    <td><?php=$_SESSION['Username'];?></td>
    <td width="25%"><input type="button" value="แก้ไขข้อมูลส่วนตัว" onclick="window.location='edit_member.php?Username=<?php=$_SESSION['Username'];?>';" /></td>
    <td width="60%"><input type="button" value="ออกจากระบบ" onclick="window.location='?Act=Logout';" /></td>
  </tr>
</table>
<?php } ?>
</body>
</html>

