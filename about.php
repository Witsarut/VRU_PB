<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
    mainmenuid: "templatemo_menu", //menu DIV id
    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
    classname: 'ddsmoothmenu', //class added to menu's outer DIV
    //customtheme: ["#1c5a80", "#18374a"],
    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
  
</head>
<body>

<div id="templatemo_wrapper">
    <div id="templatemo_header">
        <div id="site_title">
            <h1><a href="#"></a></h1>
        </div>
        <div id="templatemo_search">
            <form action="#" method="get">
              <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a href="index.php">แบบประเมิณ</a></li>
            <li><a href="about.php" class="selected">บทเรียน</a>
            </li>
            <li><a href="portfolio.php">ผู้ดูแลระบบ</a>
            </li>
            <li><a href="blog.php">เว็บบอร์ด</a></li>
            <li><a href="contact.php">แบบทดสอบ</a></li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
   
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>


<div id="templatemo_main">
    <td width="50"><h2>รายวิชาของคณะเทคโนโลยีอุตสาหกรรม</h2></td>
 
<?php
	$objConnect= mysql_connect("localhost","root","") or die ("Error connect fail");
	$objDB = mysql_select_db("vru-database");
	mysql_query("SET NAMES UTF8");
	$strSQL="SELECT * FROM tbl_uploads";
	$objQuery=mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
?>

<table width="900" border="1" align="center" cellpadding="1" cellspacing="1">
	<tr>	
		<th width="5%"><font color="black">ลำดับ</font></th>
		<th width="35%"><font color="black">รายวิชา</font></th>
		<th width="20%"><font color="black">อาจารย์ผู้สอน</font></th>
		<th width="15%"><font color="black">ข้อมูลประเภท</font></th>
		<th width="45%"><font color="black">สื่อการสอน/ไฟล์วิชา</font></th>
	</tr>	
<?php
	$sql="SELECT * FROM tbl_uploads";
	mysql_query("SET NAMES UTF8");
	$result_set=mysql_query($sql);
while($row=mysql_fetch_array($result_set))
{
?>	
	<tr>
		<td><div align="center"><font color="black"><?php echo $row['id']?></font></div></td>
		<td><center><font color="black"><?php echo $row['subject']?></font></center></td>
		<td><center><font color="black"><?php echo $row['Teacher']?></font></center></td>
		<td><center><font color="black"><?php echo $row['type']?></font></center></td>
		<td><div align="center"><a href="admin/uploads/<?php echo $row['file']?>" target="_blank"><font color="blue">ดูข้อมูล</font></a></div></td>
		
	</tr>
<?php
}
?>

</table>

    <br>   
        <br class="cleaner" />
</div> <!-- end of main -->

<div id="templatemo_footer">
        <br><br><br><br>     
</div>

    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>

<div id="templatemo_cr_bar_wrapper">
    <div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>

</body>
</html>