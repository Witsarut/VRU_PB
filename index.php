<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VRU</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- templatemo 343 green jelly -->
<!-- 
Green Jelly Template 
http://www.templatemo.com/preview/templatemo_343_green_jelly 
-->
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
  
</head>

<body>

<div id="templatemo_wrapper">
	<div id="templatemo_header">
        <div id="site_title">
        	<h1><a href="#"></a></h1>

        </div>
        <div id="templatemo_search">
            <form action="#" method="get">
              <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
        <div class="cleaner"></div>
    </div> <!-- end of header -->
    
    <div id="templatemo_menu" class="ddsmoothmenu">
        <ul>
            <li><a href="index.php" class="selected">แบบประเมิณ</a></li>
            <li><a href="about.php">บทเรียน</a>
            </li>
            <li><a href="portfolio.php">ผู้ดูแลระบบ</a>
          	</li>
          	<li><a href="blog.php">เว็บบอร์ด</a></li>
          	<li><a href="contact.php">แบบทดสอบ</a></li>
        </ul>
        <br style="clear: left" />
    </div> <!-- end of menu -->
    
    <div id="templatemo_slider_wrapper">
        
        <div id="slider" class="nivoSlider">
            <a href="#"><img src="images/slider/01.jpg" alt="Slider 01" title="Donec sit amet gravida quam ut condimentum risus" /></a>
            <a href="#"><img src="images/slider/02.jpg" alt="Slider 02" title="Aenean tortor vel porttitor volutpat" /></a>
            <a href="#"><img src="images/slider/03.jpg" alt="Slider 03" title="Mauris lobortis placerat sollicitudin" /></a>
            <a href="#"><img src="images/slider/04.jpg" alt="Slider 04" title="Pellentesque dignissim dapibus fermentum" /></a>
        </div>
        
        <div id="htmlcaption" class="nivo-html-caption">
        	<strong>This</strong> is an example of a HTML caption with <a href="#">a link</a>.
        </div>
    
    </div>
    
    <script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
    $('#slider').nivoSlider();
    });
    </script>


<div id="templatemo_main">
    <td width="50"><h2>แบบประเมิณความพึงพอใจของนักศึกษา</h2></td>
    <td><font color="black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สถานภาพทั่วไปของนักศึกษา</font></td>
    <br>   
<form name="frmMain" method="post" action="save.php" OnSubmit="return fncSubmit();">
    <table width="850" border="1" align="center" cellpadding="1" cellspacing="1">
        <tr>
            <td width="50">&nbsp;<font color ="black">เพศ</font></td>
            <td width="387">
            <label><input type="radio" name="rdo_gender" value="ชาย" id="rdo_gender_0" /><font color="black">ชาย</font></label>
            <label><input type="radio" name="rdo_gender" value="หญิง" id="rdo_gender_1" /><font color="black">หญิง</font></label></td>
        </tr>    
          
         <tr>
             <td>&nbsp;<font color ="black">ระดับการศึกษา</font></td>
             <td width="387">
              <label><input type="radio" name="rdo_education" value="ปริญญาตรี" id="rdo_education_2"><font color="black">ปริญญาตรี</font></label>
              <br />
              <label><input type="radio" name="rdo_education" value="สูงกว่าปริญญาตรี" id="rdo_education_3"><font color="black">สูงกว่าปริญญาตรี</font></label>
        </tr> 
        
        <tr>
            <td><font color="black">&nbsp;สถานภาพ</font></td>
            <td>
            <label><input type="radio" name="rdo_state" value="อาจารย์" id="rdo_state_1" /><font color="black">อาจารย์</font></label>
            <br />    
            <label><input type="radio" name="rdo_state" value="นักเรียน/นักศึกษา" id="rdo_state_0" /><font color="black">นักเรียน/นักศึกษา</font></label>
        </tr>

</table>
<br />
    <tr>
        <td>&nbsp;<font color="black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ความพึงพอใจของผู้ใช้เว็บไซต์</font></td>
        <br />
    </tr>

<table width="850" border="1" align="center" cellpadding="1" cellspacing="1">
    <tr>
        <td width="50%"><font color="black"><center>รายการ</center></font></td>
        <td width="10%"><font color="black"><center>มากที่สุด</center></font></td>
        <td width="10%"><font color="black"><center>มาก</font></font></td>
        <td width="10%"><font color="black"><center>ปานกลาง</center></font></td>
        <td width="10%"><font color="black"><center>น้อย</center></font></td>
        <td width="10%"><font color="black"><center>น้อยที่สุด</center></font></td>
  </tr>
  
  
  
<script language="JavaScript">
	function fncSubmit()
	{
		
		if(document.frmMain.rdo_gender_0.checked == false && document.frmMain.rdo_gender_1.checked == false)
	{
		alert('กรุณาระบุ เพศ');
		return false;
	}	
	
	
		if(document.frmMain.rdo_education_2.checked == false && document.frmMain.rdo_education_3.checked == false )
	{
		alert('กรุณาระบุ ระดับการศึกษาสูงสุด');
		return false;
	}

	
	if(document.frmMain.rdo_state_0.checked == false && document.frmMain.rdo_state_1.checked == false)
	{
		alert('กรุณาระบุ  สถานะภาพ');
		return false;
	}
	
	
	var Rows = document.frmMain.hdnRows.value;
		for(x=1;x<=Rows;x++)
		{
			var op1 = document.getElementById("radionNo"+x+"_1");
			var op2 = document.getElementById("radionNo"+x+"_2");
			var op3 = document.getElementById("radionNo"+x+"_3");
			var op4 = document.getElementById("radionNo"+x+"_4");
			var op5 = document.getElementById("radionNo"+x+"_5");
			if(op1.checked == false && op2.checked == false && op3.checked == false  && op4.checked == false  && op5.checked == false)
			{
				alert('กรุณาเลือกคำตอบที่ ' + x);
				return false;
			}
		}

	}
</script>

  
  <?php
    include('conn-database.php');
    $strSQL="SELECT * FROM tb_question";
    mysql_query("SET NAMES UTF8");
    $objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
    $Num_Rows = mysql_num_rows($objQuery);
    $i=1;
    while($result2 = mysql_fetch_array($objQuery))
       {
    $id_chk = $result2['id_question'];
    $name = $result2['question'];
?>
    <tr>
      <td width="574"><font color="black"><?=$name?></font></td>
      <td width="70" align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_1" type="radio" value="5"></td>
      <td width="63" align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_2" type="radio" value="4"></td>
      <td width="71" align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_3" type="radio" value="3"></td>
      <td width="65" align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_4" type="radio" value="2"></td>
      <td width="81" align="center"><input name="radionNo<?=$i;?>" id="radionNo<?=$i;?>_5" type="radio" value="1"></td>
    </tr>
<?php  
    $i++;
    }
?>  

</table>

<table width="850"  border="1" align="center">
    <tr>
        <td><font color ='black'>ข้อเสนอแนะ</font></div></td>
    </tr>

    <tr>
        <td><textarea name="comment" cols="100" rows="4" id="comment"></textarea></td>
    </tr>
</table>    

<br/><br/>
<input type="hidden" name="hdnRows" value="<?=$i-1;?>">
<center><br/><input type="submit" name="Submit" value="ตอบแบบสอบถาม"></center> 

       


</form>
		<br class="cleaner" />
</div> <!-- end of main -->

<div id="templatemo_footer">
        <br><br><br><br>     
</div>

    <div class="cleaner"></div>
</div> <!-- end of footer -->

</div>

<div id="templatemo_cr_bar_wrapper">
	<div id="templatemo_cr_bar">
       <a href="#">มหาวิทยาลัยราชภัฏวไลยอลงกรณ์ ในพระบรมราชูปถัมป์ คณะเทคโนโลยีอุตสาหกรรม</a>
    </div>
</div>

</body>
</html>